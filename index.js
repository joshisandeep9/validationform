function JOSHIFORM()                                    
{ 
  var name = document.forms.RegForm.Name;    
  var address = document.forms.RegForm.Address;          
  var email = document.forms.RegForm.EMail;  
  var password = document.forms.RegForm.Password;   
  var password2 = document.forms.RegForm.RePassword; 
  var phone = document.forms["RegForm"]["Telephone"];  
  var sub = document.forms["RegForm"]["Subject"];  
  
  if(validatename(name)){
    if(validateaddress(address)){
      if(validateemail(email)){
        if(validatepass(password)){
          if(validatepass2(password,password2)){
            if(validatemobile(phone)){
              if(validatesubject(sub)){
                return true;
              }
            }
          }
        }
      } 

    } 
    
  }
  return false;
}
  

// Function to Validate Name
function validatename(name1)     
{
  var alphaExp = /^[0-9a-zA-Z]+$/;
  
  if(name1.value.length == 0) 
  { 
    document.getElementById("p1").innerText = "* Name field can't be empty *";
    return false; 
  } 
  

  else
  {
    if(specialchrcheck(name1.value))
    {
      document.getElementById("p1").innerText = "Special characters not allowed";
      return false;
    }

    else if(numericvalue(name1.value))
    {
      document.getElementById("p1").innerText = "* Numeric value not Allowed...*";
      return false;
    }
    document.getElementById("p1").innerText = "";
    return true;
  }

   
}
  
 
// Function to validate Address
function validateaddress(address1)       
{
  if(address1.value.length == 0) 
  { 
    document.getElementById("p2").innerText = "* Address field can't be empty *";
    return false; 
  }

  else if(address1.value.length <=10 || address1.value.length >=30)
  {
    document.getElementById("p2").innerText = "* Address length must between 10 - 30 words*";
    return false;
  }

  else
  {
    document.getElementById("p2").innerText = "";
    return true;
  }
}
   

// Function to validate Email
function validateemail(email1)          
{
  if (email1.value == "")                                   
  { 
    document.getElementById("p3").innerText = "* Emailid field can't be empty *";
    return false;
  } 
 
  else if (email1.value.indexOf("@", 0) < 0)                 
  { 
    document.getElementById("p3").innerText = "* Email Id must contain @ *";
    return false;
  } 
  
  else if (email1.value.indexOf(".", 0) < 0)                 
  { 
    document.getElementById("p3").innerText = "* Email id must contain . *";
    return false;
  }  

  else
  {
    document.getElementById("p3").innerText = "";
    return true;
  }
}

// Function to Validate Password
function validatepass(password1)
{
  if(password1.value == "") 
  { 
    document.getElementById("p4").innerText = "* Please enter your Password *";
    return false; 
  }
  else if(password1.value.length <=8)
  {
    document.getElementById("p4").innerText = "* Password must be greater than 8 character*";
    return false;
  }
  else
  {
    document.getElementById("p4").innerText = "";
    return true;
  }
}

// Function to Validate second Password 
function validatepass2(pass,repass)
{
  if(repass.value == "") 
  { 
    document.getElementById("p5").innerText = "* Please enter your Password again *";
    return false; 
  }
  
  else if(pass.value.localeCompare(repass.value) == -1)
  {
    document.getElementById("p5").innerText = "* Password Mismatch... *";
    return false;
  }

  else
  {
    document.getElementById("p5").innerText = "";
    return true;
  }
}

// Function to validate Mobile No
function validatemobile(phone1)
{
  var phoneno = /^\d{10}$/;
  if(phone1.value == "") 
  { 
    document.getElementById("p6").innerText = "* Mobile No field can't be empty *";
    return false; 
  }
  
  if(phone1.value.match(phoneno))
  {
    document.getElementById("p6").innerText = "";
    return true;
  }
  else
  {
    document.getElementById("p6").innerText = "* Only 10 digit Numeric value Allowed *";
    return false; 
  } 

}

// Function to check Subject
function validatesubject(sub) 
{
  if(sub.value=="Branch")
  {
    document.getElementById("p7").innerText = "* Please select one option *";
    return false; 
  }
  else
  {
    document.getElementById("p7").innerText = "";
    true;
  }

}
 
// function to check special Characters
function specialchrcheck(data)
{
  var splchr = ["~","!","@","#","$","%","^","&","*","()","_",":","+","-"];
  let n;
  for(var i=0;i<data.length;i++)
  {
	  for(j=0;j<4;j++)
    {
    	if(data[i] == splchr[j])
        {
        	n = true;
        	break;
        }
        else
        {
        	 continue;
        }
    } 
    if(n == true)
    break;
    else
    n = false;
    continue;
  }
  return n; 
}

// function to check numeric no is present or not
function numericvalue(data)
{
  var numchar = ["0","1","2","3","4","5","6","7","8","9"];
  let n;
  for(var i=0;i<data.length;i++)
  {
	  for(j=0;j<4;j++)
    {
    	if(data[i] == numchar[j])
        {
        	n = true;
        	break;
        }
        else
        {
        	 continue;
        }
    } 
    if(n == true)
    break;
    else
    n = false;
    continue;
  }
  return n;
}





    
    
    
  
    
  

  

  




  








